package cz.sazonov.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("cz.sazonov.service")
public class RootConfig {

}

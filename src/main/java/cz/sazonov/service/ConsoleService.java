package cz.sazonov.service;

import cz.sazonov.model.Balance;
import cz.sazonov.util.BalanceParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.System.exit;

@Service
public class ConsoleService {

    @Autowired
    private BalanceService balanceService;

    @Autowired
    private PrinterThreadService printerThreadService;

    private final String WELCOME_TEXT = "Welcome into PaymentTracker application!\nYou can exit at any time using [quit] command.";
    private final String INIT_FILE_MESSAGE = "Please specify the path to the file with initial data or type [skip] if you do not wish to load initial data:";
    private final String FILE_NOT_FOUND = "File doesn't exist!";
    private final String GENERAL_IO_ERROR = "File can't be opened!";
    private final String FILE_CORRUPTED = "File is corrupted! There is an error in the text: %s (line %d)";
    private final String INVALID_INPUT = "Invalid input!";
    private final String NEW_PAYMENT = "Type new payment in format: [code] [amount]";
    private final String BALANCE_UPDATED = "Total balance of %s was updated: %s";
    private final String FILE_SUCCESS = "File loaded successfully!";


    private final String QUIT_COMMAND = "quit";
    private final String SKIP_COMMAND = "skip";

    Scanner scanner = new Scanner(System.in);

    /**
     * Starting communication with user
     */
    public void start() {
        // load initial data from file and start printer thread
        System.out.println(WELCOME_TEXT);
        loadInitialData();
        printerThreadService.start();
        System.out.println(NEW_PAYMENT);

        // read new payments from console until command quit is passed
        while (true) {
            String command = readCommand();
            try {
                Balance balance = balanceService.parse(command);
                Balance newBalance = balanceService.add(balance);
                System.out.println(String.format(BALANCE_UPDATED, newBalance.getCode(), newBalance));
            } catch (Exception e) {
                System.out.println(INVALID_INPUT);
            }
        }
    }

    /**
     * Load initial data from file
     */
    private void loadInitialData() {
        while (true) {
            System.out.println(INIT_FILE_MESSAGE);
            String command = readCommand();
            if (SKIP_COMMAND.equals(command) || readFile(command)) {
                return;
            }
        }
    }

    /**
     * Read one line from console and check whether it equals to quit command
     * @return one line from console
     */
    private String readCommand() {
        String command = scanner.nextLine();
        if (QUIT_COMMAND.equals(command)) {
            exit(0);
        }
        return command;
    }

    /**
     * Read and parse initial file
     * @param fileName
     * @return true if file successfully loaded and parsed, false if there is some error
     */
    private boolean readFile(String fileName) {
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            List<String> stringList = stream.collect(Collectors.toList());
            List<Balance> balances = parseBalances(stringList);
            balances.forEach(balanceService::add);
            System.out.println(FILE_SUCCESS);
            balanceService.print();
            return true;
        } catch (NoSuchFileException e) {
            System.out.println(FILE_NOT_FOUND);
        } catch (IOException e1) {
            e1.printStackTrace();
            System.out.println(GENERAL_IO_ERROR);
        } catch (BalanceParseException e2) {
            System.out.println(e2.getMessage());
        }
        return false;
    }

    /**
     *
     * @param list of lines
     * @return list of balances
     * @throws BalanceParseException
     */
    private List<Balance> parseBalances(List<String> list) throws BalanceParseException {
        // TODO: consider to rewrite with @Validated, @Valid using MethodValidationPostProcessor
        List<Balance> balances = new LinkedList<>();
        for (int i = 0; i < list.size(); i++) {
            balances.add(parseBalance(list.get(i), i));
        }
        return balances;
    }

    private Balance parseBalance(String line, int lineNumber) throws BalanceParseException {
        try {
            return balanceService.parse(line);
        } catch (Exception e) {
            throw new BalanceParseException(String.format(FILE_CORRUPTED, line, lineNumber));
        }
    }
}

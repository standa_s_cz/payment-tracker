package cz.sazonov.service;

import cz.sazonov.model.Balance;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.*;
import java.math.BigDecimal;
import java.util.*;

@Service
@Validated
public class BalanceService {

    private Map<String, Balance> balanceMap = new LinkedHashMap<>();

    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    private String CURRENT_BALANCE = "=== Current balance (%s) ===";

    /**
     * Synchronized add method
     *
     * @param newBalance
     * @return
     */
    public Balance add(Balance newBalance) {
        synchronized (this) {
            Balance balance = balanceMap.get(newBalance.getCode());
            if (balance == null) {
                balance = newBalance;
                balanceMap.put(newBalance.getCode(), newBalance);
            } else {
                BigDecimal newAmount = balance.getAmount().add(newBalance.getAmount());
                balance.setAmount(newAmount);
            }
            return balance;
        }
    }

    public void print() {
        synchronized (this) {
            System.out.println(String.format(CURRENT_BALANCE, new Date()));
            balanceMap.forEach((key, value) -> {
                if (!BigDecimal.ZERO.equals(value.getAmount())) {
                    System.out.println(value);
                }
            });
        }
    }

    public Balance parse(String line) {
        Scanner scanner = new Scanner(line);
        String code = scanner.next().toUpperCase();
        BigDecimal value = new BigDecimal(scanner.next());
        Balance balance = new Balance(code, value);
        validate(balance);
        return balance;
    }

    private void validate(Balance balance) {
        Set<ConstraintViolation<Balance>> set = validator.validate(balance);
        if (set.size() > 0) {
            throw new RuntimeException("Constraint violation!");
        }
    }

    public int size() {
        return balanceMap.size();
    }

    public Balance get(String code) {
        return balanceMap.get(code);
    }
}

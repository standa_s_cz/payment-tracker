package cz.sazonov.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("prototype")
public class PrinterThreadService extends Thread {

    @Autowired
    private BalanceService balanceService;

    private final Long SLEEP_DURATION = 60_000L;

    /**
     * Print balance one time per minute
     */
    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(SLEEP_DURATION);
                balanceService.print();
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
        }
    }
}

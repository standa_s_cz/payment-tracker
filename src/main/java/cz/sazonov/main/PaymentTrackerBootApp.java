package cz.sazonov.main;

import cz.sazonov.config.RootConfig;
import cz.sazonov.service.ConsoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(RootConfig.class)
public class PaymentTrackerBootApp implements CommandLineRunner {

    @Autowired
    private ConsoleService consoleService;

    public static void main(String[] args) throws Exception {
        SpringApplication app = new SpringApplication(PaymentTrackerBootApp.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }

    @Override
    public void run(String... args) throws Exception {
        consoleService.start();
    }
}
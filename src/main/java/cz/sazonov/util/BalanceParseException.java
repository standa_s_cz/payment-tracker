package cz.sazonov.util;

public class BalanceParseException extends Exception {
    public BalanceParseException(String message) {
        super(message);
    }
}

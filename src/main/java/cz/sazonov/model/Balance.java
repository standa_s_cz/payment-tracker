package cz.sazonov.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.*;
import java.math.BigDecimal;

public class Balance {

    @NotEmpty
    @Size(min = 3, max = 3)
    @Pattern(regexp = "[a-zA-Z]+")
    private String code;

    @NotNull
    private BigDecimal amount;

    public Balance(String code, BigDecimal amount) {
        this.code = code;
        this.amount = amount;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return code + " " + amount;
    }
}

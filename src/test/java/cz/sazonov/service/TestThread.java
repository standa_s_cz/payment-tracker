package cz.sazonov.service;

import cz.sazonov.model.Balance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@Scope("prototype")
public class TestThread extends Thread {

    @Autowired
    private BalanceService balanceService;

    @Override
    public void run() {
        System.out.println("Thread: " + this.toString() + " started!");
        for (int i = 0; i < 1000; i++) {
            balanceService.add(new Balance("USD", new BigDecimal(1)));
            balanceService.add(new Balance("CZK", new BigDecimal(2)));
        }
        System.out.println("Thread: " + this.toString() + " finished!");
    }
}
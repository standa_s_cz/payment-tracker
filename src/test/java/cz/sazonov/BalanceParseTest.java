package cz.sazonov;

import cz.sazonov.config.RootConfig;
import cz.sazonov.model.Balance;
import cz.sazonov.service.BalanceService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@Import(RootConfig.class)
public class BalanceParseTest {

    @Autowired
    private BalanceService balanceService;


    @Test
    public void testParse() {
        Assert.assertNull(parse("ABC1234"));
        Assert.assertNull(parse("A-C 12345"));

        Balance b1 = parse("ABC 1234");
        Assert.assertEquals(new BigDecimal("1234"), b1.getAmount());
        Assert.assertEquals("ABC", b1.getCode());

        Balance b2 = parse("aBc    123.456");
        Assert.assertEquals(new BigDecimal("123.456"), b2.getAmount());
        Assert.assertEquals("ABC", b2.getCode());
    }

    private Balance parse(String text) {
        try {
            return balanceService.parse(text);
        } catch (Exception e) {
            return null;
        }
    }
}

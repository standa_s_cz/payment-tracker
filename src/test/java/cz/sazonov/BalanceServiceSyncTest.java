package cz.sazonov;

import cz.sazonov.config.RootConfig;
import cz.sazonov.service.BalanceService;
import cz.sazonov.service.TestThread;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@Import(RootConfig.class)
public class BalanceServiceSyncTest {

    @Autowired
    private BalanceService balanceService;

    @Autowired
    private TestThread thread1;

    @Autowired
    private TestThread thread2;

    /**
     * Remove synchronized block in cz.sazonov.service.BalanceService#add method and this test will not pass
     *
     * @throws InterruptedException
     */
    @Test
    public void testAddMethodSync() throws InterruptedException {
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        balanceService.print();
        Assert.assertEquals(2, balanceService.size());
        Assert.assertEquals(new BigDecimal(2000), balanceService.get("USD").getAmount());
        Assert.assertEquals(new BigDecimal(4000), balanceService.get("CZK").getAmount());
    }
}

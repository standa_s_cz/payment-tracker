# Developed with
jdk1.8.0_131, apache-maven-3.5.0

# Build and run
mvn package
java -jar target/payment-tracker-1.0.jar

# Used frameworks
Spring Boot, Hibernate Validator

# Testing
There are 2 unit tests

# Test files
There are 2 files for testing: in (this is ok), err (it's corrupted)

# Possible output
Welcome into PaymentTracker application!
You can exit at any time using [quit] command.
Please specify the path to the file with initial data or type [skip] if you do not wish to load initial data:
sdgddsg
File doesn't exist!
Please specify the path to the file with initial data or type [skip] if you do not wish to load initial data:
c:\err
File is corrupted! There is an error in the text: RMB2000 (line 3)
Please specify the path to the file with initial data or type [skip] if you do not wish to load initial data:
c:\in
File loaded successfully!
=== Current balance (Wed Sep 20 17:46:18 CEST 2017) ===
USD 900
HKD 300
RMB 2000
Type new payment in format: [code] [amount]
usd -500
Total balance of USD was updated: USD 400
usd -400
Total balance of USD was updated: USD 0
CZK 250
Total balance of CZK was updated: CZK 250
=== Current balance (Wed Sep 20 17:47:18 CEST 2017) ===
HKD 300
RMB 2000
CZK 250
quit